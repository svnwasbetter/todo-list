import { Pipe, PipeTransform } from '@angular/core';
import {Todo} from "./todo.service";

@Pipe({
    name: 'SearchFilterPipe',
    pure: false
})
export class SearchFilterPipe implements PipeTransform {
    transform(items: Todo[], filter: string): any {
        if (!items || !filter) {
            return items;
        }
        return items.filter(item => item.task.includes(filter));
    }
}