import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

export interface Todo {
    id: number;
    task: string;
    priority: 1 | 2 | 3;
}

@Injectable({
    providedIn: 'root'
})
export class TodoService {
    constructor(private http: HttpClient) {
    }

    getAll(): Observable<Todo[]> {
        return this.http.get<Todo[]>("http://localhost:8099/api/todos");
    }

    remove(id: number): Observable<any> {
        return this.http.delete("http://localhost:8099/api/todos/" + id);
    }
}
