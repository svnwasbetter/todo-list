import {Component} from '@angular/core';
import {Todo, TodoService} from "./todo.service";
import {Observable} from "rxjs";
import {take} from "rxjs/operators";

@Component({
    selector: 'app-root',
    template: `
        <div class="title">
            <h1>
                A list of TODOs
            </h1>
        </div>
        <div class="list">
            <label for="search">Search...</label>
            <input id="search" type="text" (change)="criteriaChanged($event)"
                   (durationchange)="criteriaChanged($event)">
            <div *ngIf="todos$ | async as todos; else loader">
                <app-todo-item *ngFor="let todo of todos | SearchFilterPipe:filteringText" [item]="todo"
                               (click)="removeTodo(this.todo.id)"></app-todo-item>
            </div>
            <ng-template #loader>
                <app-progress-bar></app-progress-bar>
            </ng-template>
        </div>
    `,
    styleUrls: ['app.component.scss']
})
export class AppComponent {

    todos$: Observable<Todo[]>;
    readonly todoService: TodoService;

    constructor(todoService: TodoService) {
        this.todoService = todoService;
        this.todos$ = todoService.getAll();
    }

    filteringText: string = '';

    criteriaChanged(event: Event) {
        this.filteringText = (event.target as HTMLInputElement).value;
    }

    removeTodo(id: number) {
        this.todoService.remove(id).pipe(take(1)).subscribe(
            () => this.todos$ = this.todoService.getAll(),
            () => console.log('error when trying to delete todo with id: ' + id)
        );
    }
}
